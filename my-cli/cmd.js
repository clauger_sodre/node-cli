#!/usr/bin/env node

import axios from 'axios'
import minimist from 'minimist'
import pkg from 'enquirer'
const { prompt } = pkg

const API = 'http://localhost:3000'

const usage = (msg = 'Back office for my App') => {
  console.log(`\n${msg}\b`)
  console.log(' usage: my-cli --amount= --api=')
  console.log('	       my-cli -n= --api=\n')
}

const argv = process.argv.slice(2)
const args = minimist(argv, {
  alias: {
    amount: ['n', 'amt'],
    id: ['i']
  },
  string: ['api'],
  default: { api: API }
})

if (args._.length < 1) {
  usage()
  process.exit(1)
}

//const [id, amt] = argv
//const amount = Number(amt)

const [others] = args._
const { amount, api, id } = args
const response = await prompt([
  {
    type: 'input',
    name: 'name',
    message: `What is your name?`
  },
  {
    type: 'confirm',
    name: 'confirmId',
    message: `Does id ${id} is correct?`
  },
  {
    type: 'confirm',
    name: 'confirmAmount',
    message: `Does Amount ${amount} id correct?`
  }
])

if (!response.confirId && !response.confirmAmount) {
  console.log('unconfirmed parameters')
  process.exit(1)
}
try {
  const result = await axios.post(
    `${API}/orders/`,
    {
      amount,
      id,
      others
    },
    {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  )
  console.log('others args', others)
  console.log('Response: ', result.data)
} catch (err) {
  console.log(err.message)
  process.exit(1)
}
if (Number.isInteger(amount) === false) {
  usage('Error: amount must be an integer')
  process.exit(1)
}
