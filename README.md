# Node Cli

[![NodeJs](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png)](https://nodejs.org/en/)

## Tech

- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework
- [minimist] - This module is the guts of optimist's argument parser without all the fanciful decoration.
- [enquirer] - Created by jonschlinkert and doowb, Enquirer is fast, easy to use, and lightweight enough for small projects, while also being   powerful and customizable enough for the most advanced use cases.
- [ESLint] - ESLint is an open source project that helps you find and fix problems in JavaScript code.
- [markdown-it] - Markdown parser done right. Fast and easy to extend.

## Getting started

Simple test with Node Cli


Run de server on server package
```
cd ./server
npm run start
```
In another terminal on the same root folder, send de command
n = amount
i = id

```
cd ./my-cli
node cmd.js A1 -n 5 -i 7 
```

