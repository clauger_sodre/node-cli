const express = require('express')
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.post('/orders/',async (req, res)=>{
	console.log(req.body)
	res.status(200).json({id:req.body.id,amt:req.body.amount});
});
app.get('/',async (req,res)=>{
	res.status(200).json({id:2,amt:2});
})

app.listen(3000);
